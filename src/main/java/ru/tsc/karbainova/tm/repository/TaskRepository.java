package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
    @Override
    public void add(String userId, Task task) {
        task.setUserId(userId);
        entities.add(task);
    }

    @Override
    public void remove(String userId, Task task) {
        if (!userId.equals(task.getUserId())) return;
        entities.remove(task);
    }

    @Override
    public boolean existsById(String userId, String id) {
        return findById(userId, id) != null;
    }

    @Override
    public List<Task> findAll() {
        return entities;
    }

    public List<Task> findAll(String userId) {
        final List<Task> res = new ArrayList<>();
        for (Task task : entities) {
            if (task == null) continue;
            if (userId.equals(task.getUserId())) res.add(task);
        }
        return res;
    }

    @Override
    public List<Task> findAll(String userId, Comparator comparator) {
        final List<Task> tasks = new ArrayList<>(findAll(userId));
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public void clear(String userId) {
        final List<Task> entities = findAll(userId);
        this.entities.removeAll(entities);
    }

    @Override
    public Task findById(String userId, String id) {
        for (Task task : entities) {
            if (task == null) continue;
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(String userId, int index) {
        final List<Task> entities = findAll(userId);
        return entities.get(index);
    }

    @Override
    public Task findByName(String userId, String name) {
        for (Task task : entities) {
            if (task == null) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeById(String userId, String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Override
    public Task removeByName(String userId, String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeByIndex(String userId, int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task taskUnbindById(String userId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(String userId, String projectId) {
        List<Task> listProject = findAllTaskByProjectId(userId, projectId);
        for (Task task : listProject) {
            entities.remove(task);
        }
    }

    @Override
    public Task bindTaskToProjectById(String userId, String projectId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String userId, String projectId) {
        List<Task> listTasks = new ArrayList<>();
        for (Task task : entities) {
            if (projectId.equals(task.getProjectId())) listTasks.add(task);
        }
        return listTasks;
    }
}
