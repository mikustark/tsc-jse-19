package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.model.AbstractEntity;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        List<E> list = findAll();
        list.sort(comparator);
        return list;
    }

    @Override
    public E add(E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public E findById(String userId, String id) {
        if (id == null) return null;
        for (E entity : entities) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public void clear() {
        final List<E> entities = findAll();
        this.entities.removeAll(entities);
    }

    @Override
    public E removeById(String userId, String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public void remove(E entity) {
        entities.remove(entity);
    }
}
