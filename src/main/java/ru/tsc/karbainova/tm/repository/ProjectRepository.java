package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.model.Project;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(String userId, Project project) {
        project.setUserId(userId);
        entities.add(project);
    }

    @Override
    public void remove(String userId, Project project) {
        if (!userId.equals(project.getUserId())) return;
        entities.remove(project);
    }

    @Override
    public boolean existsById(String userId, String id) {
        return findById(userId, id) != null;
    }

    @Override
    public List<Project> findAll(String userId, Comparator comparator) {
        final List<Project> projects = new ArrayList<>(findAll(userId));
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> pro = new ArrayList<>(projects);
        pro.sort(comparator);
        return pro;
    }


    @Override
    public List<Project> findAll(String userId) {
        final List<Project> pro = new ArrayList<>();
        for (Project project : entities) {
            if (project == null) continue;
            if (userId.equals(project.getUserId())) pro.add(project);
        }
        return pro;
    }

    @Override
    public void clear(String userId) {
        final List<Project> entities = findAll(userId);
        this.entities.removeAll(entities);
    }

    @Override
    public Project findById(String userId, String id) {
        for (Project project : entities) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(String userId, int index) {
        final List<Project> entities = findAll(userId);
        return entities.get(index);
    }

    @Override
    public Project findByName(String userId, String name) {
        for (Project project : entities) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeById(String userId, String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeByName(String userId, String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(String userId, int index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }
}
